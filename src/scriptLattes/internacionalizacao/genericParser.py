#!/usr/bin/python
from future import standard_library
standard_library.install_aliases()

import re
import urllib.request, urllib.parse, urllib.error
import  unicodedata
from HTMLParserNew import HTMLParser

class genericParser(HTMLParser):
    def __init__(self,parserField):
        HTMLParser.__init__(self)
        self.recording = 0 
        self.data = []
        self.field = parserField

    def handle_starttag(self, tag, attrs):
        if tag==self.field[1]:
           for attr in attrs:
                if self.field[2] == attr[0] and attr[1]==self.field[3]:
                    self.recording = 1

    def handle_endtag(self, tag):
        if tag == self.field[1]:
            if self.recording == 1:
                self.recording -=1

    def handle_data(self, data):
        if self.recording:			
            data = ''.join((c for c in unicodedata.normalize('NFD', data) if unicodedata.category(c) != 'Mn'))
            self.data.append(data)
    

    
