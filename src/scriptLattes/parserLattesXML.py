#!/usr/bin/python
# encoding: utf-8
# filename: parserLattesXML.py
#
#  scriptLattes V8
#  Copyright 2005-2013: Jesús P. Mena-Chalco e Roberto M. Cesar-Jr.
#  http://scriptlattes.sourceforge.net/
#
#
#  Este programa é um software livre; você pode redistribui-lo e/ou 
#  modifica-lo dentro dos termos da Licença Pública Geral GNU como 
#  publicada pela Fundação do Software Livre (FSF); na versão 2 da 
#  Licença, ou (na sua opinião) qualquer versão.
#
#  Este programa é distribuído na esperança que possa ser util, 
#  mas SEM NENHUMA GARANTIA; sem uma garantia implicita de ADEQUAÇÂO a qualquer
#  MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
#  Licença Pública Geral GNU para maiores detalhes.
#
#  Você deve ter recebido uma cópia da Licença Pública Geral GNU
#  junto com este programa, se não, escreva para a Fundação do Software
#  Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
from future import standard_library
standard_library.install_aliases()
from builtins import chr

import html.parser
import re
import string
from tidylib import tidy_document
from html.entities import name2codepoint

from html.parser import HTMLParser
from formacaoAcademica import *
from areaDeAtuacao import *
from idioma import *
from premioOuTitulo import *
from projetoDePesquisa import *

from artigoEmPeriodico import *
from livroPublicado import *
from capituloDeLivroPublicado import *
from textoEmJornalDeNoticia import *
from trabalhoCompletoEmCongresso import *
from resumoExpandidoEmCongresso import *
from resumoEmCongresso import *
from artigoAceito import *
from apresentacaoDeTrabalho import *
from outroTipoDeProducaoBibliografica import *

from softwareComPatente import *
from softwareSemPatente import *
from produtoTecnologico import *
from processoOuTecnica import *
from trabalhoTecnico import *
from outroTipoDeProducaoTecnica import *

from patentesRegistros.patente import *
from patentesRegistros.programaComputador import *
from patentesRegistros.desenhoIndustrial import *

from producaoArtistica import *
from orientacaoEmAndamento import *
from orientacaoConcluida import *
from organizacaoDeEvento import *
from participacaoEmEvento import *

class ParserLattesXML(HTMLParser):
	item = None
	nomeCompleto = ''
	bolsaProdutividade = ''
	enderecoProfissional = ''
	sexo = ''
	nomeEmCitacoesBibliograficas = ''
	atualizacaoCV = ''
	foto = ''
	textoResumo = ''
	idLattes = ''
	url = ''

	listaIDLattesColaboradores = []
	listaFormacaoAcademica = []
	listaProjetoDePesquisa = []
	listaAreaDeAtuacao = []
	listaIdioma = []
	listaPremioOuTitulo = []

	listaArtigoEmPeriodico = []
	listaLivroPublicado = []
	listaCapituloDeLivroPublicado = []
	listaTextoEmJornalDeNoticia = []
	listaTrabalhoCompletoEmCongresso = []
	listaResumoExpandidoEmCongresso = []
	listaResumoEmCongresso = []
	listaArtigoAceito = []
	listaApresentacaoDeTrabalho = []
	listaOutroTipoDeProducaoBibliografica = []

	listaSoftwareComPatente = []
	listaSoftwareSemPatente = []
	listaProdutoTecnologico = []
	listaProcessoOuTecnica = []
	listaTrabalhoTecnico = []
	listaOutroTipoDeProducaoTecnica = []
	listaProducaoArtistica = []

	# Patentes e registros
	listaPatente = []
	listaProgramaComputador = []
	listaDesenhoIndustrial = []

	# Orientaçoes em andamento (OA)
	listaOASupervisaoDePosDoutorado = []
	listaOATeseDeDoutorado = []
	listaOADissertacaoDeMestrado = []
	listaOAMonografiaDeEspecializacao = []
	listaOATCC = []
	listaOAIniciacaoCientifica = []
	listaOAOutroTipoDeOrientacao = []

	# Orientações concluídas (OC)
	listaOCSupervisaoDePosDoutorado = []
	listaOCTeseDeDoutorado = []
	listaOCDissertacaoDeMestrado = []
	listaOCMonografiaDeEspecializacao = []
	listaOCTCC = []
	listaOCIniciacaoCientifica = []
	listaOCOutroTipoDeOrientacao = []
                
	# Eventos
	listaParticipacaoEmEvento = []
	listaOrganizacaoDeEvento = []

	# variáveis auxiliares

	achouPremioOuTitulo = None
	achouArtigoEmPeriodico = None #
	achouCapituloDeLivroPublicado = None #
	achouLivroPublicado = None #
	achouTextoEmJornalDeNoticia = None #
	achouTrabalhoCompletoEmCongresso = None #
	achouResumoExpandidoEmCongresso = None #
	achouResumoEmCongresso = None #
	achouArtigoAceito = None #
	achouApresentacaoDeTrabalho = None # 
	achouOutroTipoDeProducaoBibliografica = None #

	achouSoftwareComPatente = None
	achouSoftwareSemPatente = None
	achouProdutoTecnologico = None
	achouProcessoOuTecnica = None
	achouTrabalhoTecnico = None
	achouOutroTipoDeProducaoTecnica = None

	achouProducaoArtistica = None
	achouTrabalhoEmEvento = None

	achouOCDissertacaoDeMestrado = None
	achouOCSupervisaoDePosDoutorado = None
	achouOCTeseDeDoutorado = None
	achouOADissertacaoDeMestrado = None
	achouOASupervisaoDePosDoutorado = None
	achouOATeseDeDoutorado = None

	achouOrientacoes = None

	achouProjetoDePesquisa = None
	procurarCabecalho = None
	partesDoItem = []

# ------------------------------------------------------------------------ #
	def __init__(self, idMembro, cvLattesXML):
		HTMLParser.__init__(self)

		# inicializacao obrigatoria
		self.idMembro = idMembro
		self.sexo = ''
		self.nomeCompleto = '[Nome-nao-identificado]'

		self.item = ''
		self.listaIDLattesColaboradores = []
		self.listaFormacaoAcademica = []
		self.listaProjetoDePesquisa = []
		self.listaAreaDeAtuacao = []
		self.listaIdioma = []
		self.listaPremioOuTitulo = []

		self.listaArtigoEmPeriodico = []
		self.listaLivroPublicado = []
		self.listaCapituloDeLivroPublicado = []
		self.listaTextoEmJornalDeNoticia = []
		self.listaTrabalhoCompletoEmCongresso = []
		self.listaResumoExpandidoEmCongresso = []
		self.listaResumoEmCongresso = []
		self.listaArtigoAceito = []
		self.listaApresentacaoDeTrabalho = []
		self.listaOutroTipoDeProducaoBibliografica = []

		self.listaSoftwareComPatente = []
		self.listaSoftwareSemPatente = []
		self.listaProdutoTecnologico = []
		self.listaProcessoOuTecnica = []
		self.listaTrabalhoTecnico = []
		self.listaOutroTipoDeProducaoTecnica = []
		self.listaProducaoArtistica = []

		# Patentes e registros
		self.listaPatente = []
		self.listaProgramaComputador = []
		self.listaDesenhoIndustrial = []

		self.listaOASupervisaoDePosDoutorado = []
		self.listaOATeseDeDoutorado = []
		self.listaOADissertacaoDeMestrado = []
		self.listaOAMonografiaDeEspecializacao = []
		self.listaOATCC = []
		self.listaOAIniciacaoCientifica = []
		self.listaOAOutroTipoDeOrientacao = []

		self.listaOCSupervisaoDePosDoutorado = []
		self.listaOCTeseDeDoutorado = []
		self.listaOCDissertacaoDeMestrado = []
		self.listaOCMonografiaDeEspecializacao = []
		self.listaOCTCC = []
		self.listaOCIniciacaoCientifica = []
		self.listaOCOutroTipoDeOrientacao = []

		self.listaParticipacaoEmEvento = []
		self.listaOrganizacaoDeEvento = []

		# inicializacao 
		self.idLattes = ''
		self.url      = ''
		self.foto     = ''
		self.idOrientando = ''

		# feed it!
		self.feed(cvLattesXML)

	# ------------------------------------------------------------------------ #
	def handle_starttag(self, tag, attributes):

		if tag=='curriculo-vitae':
			for name, value in attributes:
				if name=='data-atualizacao':
                                        dataCV = datetime.datetime.strptime(value, "%d%m%Y").date()
                                        self.atualizacaoCV = dataCV.strftime("%d/%m/%Y")

				if name=='numero-identificador':
					self.idLattes = value
					self.url = 'http://lattes.cnpq.br/'+value
					self.url = self.url.encode('utf-8')

		if tag=='dados-gerais':
			for name, value in attributes:
				if name=='nome-completo':
					self.nomeCompleto = value
				if name=='nome-em-citacoes-bibliograficas':
					self.nomeEmCitacoesBibliograficas = value
				if name=='sexo':
					self.sexo = value.capitalize()
		
		if tag=='resumo-cv':
			for name, value in attributes:
				if name=='texto-resumo-cv-rh':
					self.textoResumo = value

		# new --------- chris --------------

		if tag=='premio-titulo':
			self.achouPremioOuTitulo = 1
			self.descricao = ''
			self.ano = ''

		if tag=='projeto-de-pesquisa':
			self.achouProjetoDePesquisa = 1
			self.nome = ''
			self.descricao = list([])
			self.anoinicio = ''
			self.anofim = ''

		# ----------------------------------:w

		# por implementar

		#if tag=='FORMACAO-ACADEMICA-TITULACAO':
		#	for name, value in attributes:
		#		if name=='':
		#			= value

		#if tag=='AREAS-DE-ATUACAO':
		#	for name, value in attributes:
		#		if name=='':
		#			= value

		#if tag=='IDIOMAS
		#	for name, value in attributes:
		#		if name=='':
		#			= value

		if tag=='artigo-publicado':
			self.achouArtigoEmPeriodico = 1
			self.autoresLista = list(['']*150)
			self.autores  = ''
			self.titulo   = ''
			self.ano      = ''
			self.revista  = ''
			self.volume   = ''
			self.numero   = ''
			self.paginas  = ''
			self.doi      = ''

		if tag=='livro-publicado-ou-organizado':
			self.achouLivroPublicado = 1
			self.autoresLista = list(['']*150)
			self.autores  = '' 
			self.titulo   = '' 
			self.edicao   = '' 
			self.ano      = '' 
			self.volume   = '' 
			self.paginas  = ''

		if tag=='capitulo-de-livro-publicado':
			self.achouCapituloDeLivroPublicado = 1
			self.autoresLista = list(['']*150)
			self.autores  = '' 
			self.titulo   = '' 
			self.livro    = '' 
			self.edicao   = '' 
			self.editora  = '' 
			self.ano      = '' 
			self.volume   = '' 
			self.paginas  = ''

		if tag=='texto-em-jornal-ou-revista':
			self.achouTextoEmJornalDeNoticia = 1
			self.autoresLista = list(['']*150)
			self.autores  = '' 
			self.titulo   = '' 
			self.nomeJornal= '' 
			self.data     = '' 
			self.volume   = '' 
			self.paginas  = ''
			self.ano      = '' 

		if tag=='trabalho-em-eventos':
			self.achouTrabalhoEmEvento = 1
			self.autoresLista = list(['']*150)
			self.autores  = '' 
			self.titulo   = '' 
			self.nomeDoEvento= '' 
			self.ano      = '' 
			self.volume   = '' 
			self.numero   = '' 
			self.paginas  = ''
			self.doi      = ''

		if tag=='artigo-aceito-para-publicacao':
			self.achouArtigoAceito = 1
			self.autoresLista = list(['']*150)
			self.autores  = '' 
			self.titulo   = '' 
			self.revista  = '' 
			self.ano      = '' 
			self.volume   = '' 
			self.numero   = '' 
			self.paginas  = ''
			self.doi      = ''

		if tag=='apresentacao-de-trabalho':
			self.achouApresentacaoDeTrabalho = 1
			self.autoresLista = list(['']*150)
			self.autores  = '' 
			self.titulo   = '' 
			self.ano      = '' 
			self.natureza = '' 

		if tag=='outra-producao-bibliografica':
			self.achouOutroTipoDeProducaoBibliografica = 1
			self.autoresLista = list(['']*150)
			self.autores  = '' 
			self.titulo   = '' 
			self.ano      = '' 
			self.natureza = '' 

# ----------------------------------------------------------------------

		if tag=='orientacoes-concluidas-para-pos-doutorado':
			self.achouOCSupervisaoDePosDoutorado = 1
			self.nome = ''
			self.tituloDoTrabalho = ''
			self.ano = ''
			self.instituicao = ''
			self.agenciaDeFomento = ''
			self.tipoDeOrientacao = ''

		if tag=='orientacoes-concluidas-para-doutorado':
			self.achouOCTeseDeDoutorado = 1
			self.nome = ''
			self.tituloDoTrabalho = ''
			self.ano = ''
			self.instituicao = ''
			self.agenciaDeFomento = ''
			self.tipoDeOrientacao = ''

		if tag=='orientacoes-concluidas-para-mestrado':
			self.achouOCDissertacaoDeMestrado = 1
			self.nome = ''
			self.tituloDoTrabalho = ''
			self.ano = ''
			self.instituicao = ''
			self.agenciaDeFomento = ''
			self.tipoDeOrientacao = ''

#-----------------------------------------------------------------
# novo código - Chris, 17/1/2016

		if tag=='orientacao-em-andamento-de-pos-doutorado':
			self.achouOASupervisaoDePosDoutorado = 1
			self.nome = ''
			self.tituloDoTrabalho = ''
			self.ano = ''
			self.instituicao = ''
			self.agenciaDeFomento = ''
			self.tipoDeOrientacao = ''

		if tag=='orientacao-em-andamento-de-doutorado':
			self.achouOATeseDeDoutorado = 1
			self.nome = ''
			self.tituloDoTrabalho = ''
			self.ano = ''
			self.instituicao = ''
			self.agenciaDeFomento = ''
			self.tipoDeOrientacao = ''

		if tag=='orientacao-em-andamento-de-mestrado':
			self.achouOADissertacaoDeMestrado = 1
			self.nome = ''
			self.tituloDoTrabalho = ''
			self.ano = ''
			self.instituicao = ''
			self.agenciaDeFomento = ''
			self.tipoDeOrientacao = ''

		if tag=='trabalho-tecnico':
			self.achouTrabalhoTecnico = 1
			self.nome = ''
			self.titulo = ''
			self.ano = ''
			self.autoresLista = list(['']*150)
			self.autores = ''

		if tag=='outra-producao-tecnica':
			self.achouOutroTipoDeProducaoTecnica = 1
			self.nome = ''
			self.titulo = ''
			self.ano = ''
			self.autoresLista = list(['']*150)
			self.autores = ''

# ----------------------------------------------------------------------
		if tag=='endereco-profissional':
			for name, value in attributes:
				if name=='nome-instituicao-empresa':
					nomeIntituicao = value
				if name=='nome-unidade':
					nomeUnidade = value
				if name=='nome-orgao':
					orgao = value
				if name=='logradouro-complemento':
					logradouro = value
				if name=='cidade':
					cidade = value
				if name=='pais':
					pais = value
				if name=='uf':
					uf = value
				if name=='cep':
					cep = value
			self.enderecoProfissional = nomeIntituicao+". "+nomeUnidade+". "+orgao+". "+logradouro+" CEP "+cep+" - "+cidade+", "+uf+" - "+pais

#-----------------------------------------------------------------
# novo código - Chris, 17/1/2016

		if self.achouProjetoDePesquisa:
			if tag=='projeto-de-pesquisa':
				for name, value in attributes:
					if name=='ano-inicio':
						self.anoinicio = value
					if name=='ano-fim':
						self.anofim = value
					if name=='nome-do-projeto':
						self.nome = value
					if name=='descricao-do-projeto':
						self.descricao.append(value)

		if self.achouPremioOuTitulo:
			if tag=='premio-titulo':
				for name, value in attributes:
					if name=='nome-do-premio-ou-titulo':
                                        	self.descricao = value
					if name=='ano-da-premiacao':
						self.ano = value

# ----------------------------------------------------------------------

		if self.achouArtigoEmPeriodico:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-para-citacao':
						autorNome = value.split(';')[0]
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-do-artigo':
				for name, value in attributes:
					if name=='titulo-do-artigo':
						self.titulo = value
					if name=='ano-do-artigo':
						self.ano = value
					if name=='doi':
						self.doi = value

			if tag=='detalhamento-do-artigo':
				for name, value in attributes:
					if name=='titulo-do-periodico-ou-revista':
						self.revista = value
					if name=='volume':
						self.volume = value
					if name=='fasciculo':
						self.numero = value
					if name=='pagina-inicial':
						pagina1 = value
					if name=='pagina-final':
						pagina2 = value
				self.paginas = pagina1+'-'+pagina2


		# ----------------------------------------------------------------------
		if self.achouLivroPublicado:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-para-citacao':
						autorNome = value.split(';')[0]
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-do-livro':
				for name, value in attributes:
					if name=='titulo-do-livro':
						self.titulo = value
					if name=='ano':
						self.ano = value

			if tag=='detalhamento-do-livro':
				for name, value in attributes:
					if name=='numero-da-edicao-revisao':
						self.edicao = value
					if name=='numero-da-serie':
						self.volume = value
					if name=='numero-de-paginas':
						self.paginas = value

		# ----------------------------------------------------------------------
		if self.achouCapituloDeLivroPublicado:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-para-citacao':
						autorNome = value.split(';')[0]
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-do-capitulo':
				for name, value in attributes:
					if name=='titulo-do-capitulo-do-livro':
						self.titulo = value
					if name=='ano':
						self.ano = value

			if tag=='detalhamento-do-capitulo':
				for name, value in attributes:
					if name=='titulo-do-livro':
						self.livro = value
					if name=='numero-da-edicao-revisao':
						self.edicao = value
					if name=='nome-da-editora':
						self.editora = value
					if name=='numero-da-serie':
						self.volume = value
					if name=='pagina-inicial':
						pagina1 = value
					if name=='pagina-final':
						pagina2 = value
				self.paginas = pagina1+'-'+pagina2

		# ----------------------------------------------------------------------
		if self.achouTextoEmJornalDeNoticia:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-para-citacao':
						autorNome = value.split(';')[0]
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-do-texto':
				for name, value in attributes:
					if name=='titulo-do-texto':
						self.titulo = value
					if name=='ano-do-texto':
						self.ano = value

			if tag=='detalhamento-do-texto':
				for name, value in attributes:
					if name=='titulo-do-jornal-ou-revista':
						self.nomeJornal = value
					if name=='data-de-publicacao':
						self.data = value
					if name=='volume':
						self.volume = value
					if name=='pagina-inicial':
						pagina1 = value
					if name=='pagina-final':
						pagina2 = value
				self.paginas = pagina1+'-'+pagina2

		# ----------------------------------------------------------------------
		if self.achouTrabalhoEmEvento:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-para-citacao':
						autorNome = value.split(';')[0]
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-do-trabalho':
				for name, value in attributes:
					if name=='natureza' and value.lower()=='completo':
						self.achouTrabalhoCompletoEmCongresso = 1
					if name=='natureza' and value.lower()=='resumo':
						self.achouResumoEmCongresso = 1
					if name=='natureza' and value.lower()=='resumo_expandido':
						self.achouResumoExpandidoEmCongresso = 1


					if name=='titulo-do-trabalho':
						self.titulo = value
					if name=='ano-do-trabalho':
						self.ano = value
					if name=='doi':
						self.doi = value
			if tag=='detalhamento-do-trabalho':
				for name, value in attributes:
					if name=='nome-do-evento':
						self.nomeDoEvento = value
					if name=='volume':
						self.volume = value
					if name=='fasciculo':
						self.numero = value
					if name=='pagina-inicial':
						pagina1 = value
					if name=='pagina-final':
						pagina2 = value
				self.paginas = pagina1+'-'+pagina2

		# ----------------------------------------------------------------------
		if self.achouArtigoAceito:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-para-citacao':
						autorNome = value.split(';')[0]
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-do-artigo':
				for name, value in attributes:
					if name=='titulo-do-artigo':
						self.titulo = value
					if name=='ano-do-artigo':
						self.ano = value
					if name=='doi':
						self.doi = value

			if tag=='detalhamento-do-artigo':
				for name, value in attributes:
					if name=='titulo-do-periodico-ou-revista':
						self.revista = value
					if name=='volume':
						self.volume = value
					if name=='fasciculo':
						self.numero = value
					if name=='pagina-inicial':
						pagina1 = value
					if name=='pagina-final':
						pagina2 = value
				self.paginas = pagina1+'-'+pagina2

		# ----------------------------------------------------------------------
		if self.achouApresentacaoDeTrabalho:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-para-citacao':
						autorNome = value.split(';')[0]
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-da-apresentacao-de-trabalho':
				for name, value in attributes:
					if name=='titulo':
						self.titulo = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.natureza = value.capitalize()

			if tag=='detalhamento-da-apresentacao-de-trabalho':
				for name, value in attributes:
					if name=='nome-do-evento':
						self.nomeEvento = value

		# ----------------------------------------------------------------------
		if self.achouOutroTipoDeProducaoBibliografica:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-para-citacao':
						autorNome = value.split(';')[0]
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-de-outra-producao':
				for name, value in attributes:
					if name=='titulo':
						self.titulo = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.natureza = value.capitalize()

			if tag=='detalhamento-de-outra-producao':
				for name, value in attributes:
					if name=='editora':
						self.editora = value

		#----------------------------------------------------------------------
		if self.achouTrabalhoTecnico:
			if tag=='autores':
				for name, value in attributes:
					if name=='nome-completo-do-autor':
						autorNome = value
					if name=='ordem-de-autoria':
						autorOrdem = value
				self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-do-trabalho-tecnico':
				for name, value in attributes:
					if name=='titulo-do-trabalho-tecnico':
						self.titulo = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.natureza = value.capitalize()

		if self.achouOutroTipoDeProducaoTecnica:
			if tag=='autores':
                                for name, value in attributes:
                                        if name=='nome-completo-do-autor':
                                                autorNome = value
                                        if name=='ordem-de-autoria':
                                                autorOrdem = value
                                self.autoresLista[int(autorOrdem)] = autorNome

			if tag=='dados-basicos-de-outra-producao-tecnica':
				for name, value in attributes:
					if name=='titulo':
						self.titulo = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.natureza = value.capitalize()

		# ----------------------------------------------------------------------
		if self.achouOCSupervisaoDePosDoutorado:
			if tag=='dados-basicos-de-orientacoes-concluidas-para-pos-doutorado':
				for name, value in attributes:
					if name=='titulo':
						self.tituloDoTrabalho = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.tipoDeOrientacao = value.capitalize()
			if tag=='detalhamento-de-orientacoes-concluidas-para-pos-doutorado':
				for name, value in attributes:
					if name=='nome-do-orientado':
						self.nome = value
					if name=='nome-da-instituicao':
						self.instituicao = value
					if name=='nome-da-agencia':
						self.agenciaDeFomento = value

		# ----------------------------------------------------------------------
		if self.achouOCTeseDeDoutorado:
			if tag=='dados-basicos-de-orientacoes-concluidas-para-doutorado':
				for name, value in attributes:
					if name=='titulo':
						self.tituloDoTrabalho = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.tipoDeOrientacao = value.capitalize()
			if tag=='detalhamento-de-orientacoes-concluidas-para-doutorado':
				for name, value in attributes:
					if name=='nome-do-orientado':
						self.nome = value
					if name=='nome-da-instituicao':
						self.instituicao = value
					if name=='nome-da-agencia':
						self.agenciaDeFomento = value

		# ----------------------------------------------------------------------
		if self.achouOCDissertacaoDeMestrado:
			if tag=='dados-basicos-de-orientacoes-concluidas-para-mestrado':
				for name, value in attributes:
					if name=='titulo':
						self.tituloDoTrabalho = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.tipoDeOrientacao = value.capitalize()
			if tag=='detalhamento-de-orientacoes-concluidas-para-mestrado':
				for name, value in attributes:
					if name=='nome-do-orientado':
						self.nome = value
					if name=='nome-da-instituicao':
						self.instituicao = value
					if name=='nome-da-agencia':
						self.agenciaDeFomento = value

# ----------------------------------------------------------------------
# Chris, 16/1/2016

		if self.achouOASupervisaoDePosDoutorado:
			if tag=='dados-basicos-da-orientacao-em-andamento-de-pos-doutorado':
				for name, value in attributes:
					if name=='titulo-do-trabalho':
						self.tituloDoTrabalho = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.tipoDeOrientacao = value.capitalize()
			if tag=='detalhamento-da-orientacao-em-andamento-de-pos-doutorado':
				for name, value in attributes:
					if name=='nome-do-orientando':
						self.nome = value
					if name=='nome-instituicao':
						self.instituicao = value
					if name=='nome-da-agencia':
						self.agenciaDeFomento = value

		# ----------------------------------------------------------------------
		if self.achouOATeseDeDoutorado:
			if tag=='dados-basicos-da-orientacao-em-andamento-de-doutorado':
				for name, value in attributes:
					if name=='titulo-do-trabalho':
						self.tituloDoTrabalho = value
					if name=='ano':
						self.ano = value
					if name=='natureza':
						self.tipoDeOrientacao = value.capitalize()
			if tag=='detalhamento-da-orientacao-em-andamento-de-doutorado':
				for name, value in attributes:
					if name=='nome-do-orientando':
						self.nome = value
					if name=='nome-instituicao':
						self.instituicao = value
					if name=='nome-da-agencia':
						self.agenciaDeFomento = value

		# ----------------------------------------------------------------------
		if self.achouOADissertacaoDeMestrado:
			if tag=='dados-basicos-da-orientacao-em-andamento-de-mestrado':
				for name, value in attributes:
					if name=='titulo-do-trabalho':
						self.tituloDoTrabalho = value
					if name=='ano': 
						self.ano = value
					if name=='natureza': 
						self.tipoDeOrientacao = value.capitalize()
			if tag=='detalhamento-da-orientacao-em-andamento-de-mestrado':
				for name, value in attributes:
					if name=='nome-do-orientando':
						self.nome = value
					if name=='nome-instituicao':
						self.instituicao = value
					if name=='nome-da-agencia':
						self.agenciaDeFomento = value

# -------------------------------------------------------------------------------------

	def handle_endtag(self, tag):

		if tag=='artigo-publicado':
			self.achouArtigoEmPeriodico = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")

			pub = ArtigoEmPeriodico(self.idMembro)
			pub.autores = self.autores
			pub.titulo  = stripBlanks(self.titulo)
			pub.revista = self.revista
			pub.volume  = self.volume
			pub.paginas = self.paginas
			pub.numero  = self.numero
			pub.ano     = self.ano
			pub.chave   = self.autores
			pub.doi     = 'http://dx.doi.org/'+self.doi if not self.doi==0 else ''
			self.listaArtigoEmPeriodico.append(pub)

		# ----------------------------------------------------------------------
		if tag=='livro-publicado-ou-organizado':
			self.achouLivroPublicado = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")

			pub = LivroPublicado(self.idMembro)
			pub.autores = self.autores
			pub.titulo  = self.titulo
			pub.edicao  = self.edicao
			pub.ano     = self.ano
			pub.volume  = self.volume
			pub.paginas = self.paginas
			pub.chave   = self.autores
			self.listaLivroPublicado.append(pub)

		# ----------------------------------------------------------------------
		if tag=='capitulo-de-livro-publicado':
			self.achouCapituloDeLivroPublicado = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")

			pub = CapituloDeLivroPublicado(self.idMembro)
			pub.autores = self.autores
			pub.titulo  = stripBlanks(self.titulo)
			pub.livro   = self.livro
			pub.edicao  = self.edicao
			pub.editora = self.editora
			pub.ano     = self.ano
			pub.volume  = self.volume
			pub.paginas = self.paginas
			pub.chave   = self.autores
			self.listaCapituloDeLivroPublicado.append(pub)

		# ----------------------------------------------------------------------
		if tag=='texto-em-jornal-ou-revista':
			self.achouTextoEmJornalDeNoticia = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")

			pub = TextoEmJornalDeNoticia(self.idMembro)
			pub.autores  = self.autores
			pub.titulo   = self.titulo
			pub.nomeJornal=self.nomeJornal 
			pub.data     = self.data
			pub.volume   = self.volume
			pub.paginas  = self.paginas
			pub.ano      = self.ano
			pub.chave   = self.autores
			self.listaTextoEmJornalDeNoticia.append(pub)

		# ----------------------------------------------------------------------
		if tag=='trabalho-em-eventos':
			self.achouTrabalhoEmEvento = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")


			if self.achouResumoEmCongresso:
				self.achouResumoEmCongresso = 0
				pub = ResumoEmCongresso(self.idMembro)
				pub.autores  = self.autores
				pub.titulo   = self.titulo
				pub.nomeDoEvento=self.nomeDoEvento
				pub.ano      = self.ano
				pub.volume   = self.volume
				pub.numero   = self.numero
				pub.paginas  = self.paginas
				pub.chave   = self.autores
				pub.doi     = 'http://dx.doi.org/'+self.doi if not self.doi==0 else ''
				self.listaResumoEmCongresso.append(pub)
				return

			if self.achouResumoExpandidoEmCongresso:
				self.achouResumoExpandidoEmCongresso = 0
				pub = ResumoExpandidoEmCongresso(self.idMembro)
				pub.autores  = self.autores
				pub.titulo   = self.titulo
				pub.nomeDoEvento=self.nomeDoEvento
				pub.ano      = self.ano
				pub.volume   = self.volume
				pub.paginas  = self.paginas
				pub.chave   = self.autores
				pub.doi     = 'http://dx.doi.org/'+self.doi if not self.doi==0 else ''
				self.listaResumoExpandidoEmCongresso.append(pub)
				return

			if self.achouTrabalhoCompletoEmCongresso:
				self.achouTrabalhoCompletoEmCongresso = 0
				pub = TrabalhoCompletoEmCongresso(self.idMembro)
				pub.autores  = self.autores
				pub.titulo   = self.titulo
				pub.nomeDoEvento=self.nomeDoEvento
				pub.ano      = self.ano
				pub.volume   = self.volume
				pub.paginas  = self.paginas
				pub.chave   = self.autores
				self.listaTrabalhoCompletoEmCongresso.append(pub)
				return

		# ----------------------------------------------------------------------
		if tag=='artigo-aceito-para-publicacao':
			self.achouArtigoAceito = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")

			pub = ArtigoAceito(self.idMembro)
			pub.autores = self.autores
			pub.titulo  = stripBlanks(self.titulo)
			pub.revista = self.revista
			pub.volume  = self.volume
			pub.paginas = self.paginas
			pub.numero  = self.numero
			pub.ano     = self.ano
			pub.chave   = self.autores
			pub.doi     = 'http://dx.doi.org/'+self.doi if not self.doi==0 else ''
			self.listaArtigoAceito.append(pub)

		# ----------------------------------------------------------------------
		if tag=='apresentacao-de-trabalho':
			self.achouApresentacaoDeTrabalho = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")

			pub = ApresentacaoDeTrabalho(self.idMembro)
			pub.autores = self.autores
			pub.titulo  = stripBlanks(self.titulo)
			pub.ano     = self.ano
			pub.natureza= self.nomeEvento+'. ('+self.natureza+')'
			pub.chave   = self.autores
			self.listaApresentacaoDeTrabalho.append(pub)
		
		# ----------------------------------------------------------------------
		if tag=='outra-producao-bibliografica':
			self.achouOutroTipoDeProducaoBibliografica = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")

			pub = OutroTipoDeProducaoBibliografica(self.idMembro)
			pub.autores = self.autores
			pub.titulo  = stripBlanks(self.titulo)
			pub.ano     = self.ano
			pub.natureza= self.editora+'. ('+self.natureza+')'
			pub.chave   = self.autores
			self.listaOutroTipoDeProducaoBibliografica.append(pub)

		# ---------------------------------------------------------------------
		# Chris - 16/1/2016

		if tag=='trabalho-tecnico':
			self.achouTrabalhoTecnico = 0

			for aut in self.autoresLista:
				if not aut=='':
					self.autores+= aut +"; "
			self.autores = self.autores.strip("; ")

			trab = TrabalhoTecnico(self.idMembro)
			trab.autores  = self.autores
			trab.titulo = stripBlanks(self.titulo)
			trab.ano = self.ano
			trab.natureza= self.natureza
			trab.chave   = self.autores
			self.listaTrabalhoTecnico.append(trab)

		if tag=='outra-producao-tecnica':
                        self.achouOutroTipoDeProducaoTecnica = 0

                        for aut in self.autoresLista:
                                if not aut=='':
                                        self.autores+= aut +"; "
                        self.autores = self.autores.strip("; ")

                        trab = OutroTipoDeProducaoTecnica(self.idMembro)
                        trab.autores  = self.autores
                        trab.titulo = stripBlanks(self.titulo)
                        trab.ano = self.ano
                        trab.natureza = self.natureza
                        trab.chave   = self.autores
                        self.listaOutroTipoDeProducaoTecnica.append(trab)

		# ----------------------------------------------------------------------
		if tag=='orientacoes-concluidas-para-pos-doutorado':
			self.achouOCSupervisaoDePosDoutorado = 0

			ori = OrientacaoConcluida(self.idMembro)
			ori.nome = self.nome
			ori.tituloDoTrabalho = self.tituloDoTrabalho
			ori.ano = self.ano
			ori.instituicao = self.instituicao
			ori.agenciaDeFomento = self.agenciaDeFomento
			ori.tipoDeOrientacao = self.tipoDeOrientacao
			ori.chave = self.nome
			self.listaOCSupervisaoDePosDoutorado.append(ori)

		if tag=='orientacoes-concluidas-para-doutorado':
			self.achouOCTeseDeDoutorado = 0

			ori = OrientacaoConcluida(self.idMembro)
			ori.nome = self.nome
			ori.tituloDoTrabalho = self.tituloDoTrabalho
			ori.ano = self.ano
			ori.instituicao = self.instituicao
			ori.agenciaDeFomento = self.agenciaDeFomento
			ori.tipoDeOrientacao = self.tipoDeOrientacao
			ori.chave = self.nome
			self.listaOCTeseDeDoutorado.append(ori)

		if tag=='orientacoes-concluidas-para-mestrado':
			self.achouOCDissertacaoDeMestrado = 0

			ori = OrientacaoConcluida(self.idMembro)
			ori.nome = self.nome
			ori.tituloDoTrabalho = self.tituloDoTrabalho
			ori.ano = self.ano
			ori.instituicao = self.instituicao
			ori.agenciaDeFomento = self.agenciaDeFomento
			ori.tipoDeOrientacao = self.tipoDeOrientacao
			ori.chave = self.nome
			self.listaOCDissertacaoDeMestrado.append(ori)

		# ----------------------------------------------------------------------
		# Chris - 16/1/2016

		if tag=='orientacao-em-andamento-de-posdoutorado':
			self.achouOASupervisaoDePosDoutorado = 0

			ori = OrientacaoEmAndamento(self.idMembro)
			ori.nome = self.nome
			ori.tituloDoTrabalho = self.tituloDoTrabalho
			ori.ano = self.ano
			ori.instituicao = self.instituicao
			ori.agenciaDeFomento = self.agenciaDeFomento
			ori.tipoDeOrientacao = self.tipoDeOrientacao
			ori.chave = self.nome
			self.listaOASupervisaoDePosDoutorado.append(ori)

		if tag=='orientacao-em-andamento-de-doutorado':
			self.achouOATeseDeDoutorado = 0

			ori = OrientacaoEmAndamento(self.idMembro)
			ori.nome = self.nome
			ori.tituloDoTrabalho = self.tituloDoTrabalho
			ori.ano = self.ano
			ori.instituicao = self.instituicao
			ori.agenciaDeFomento = self.agenciaDeFomento
			ori.tipoDeOrientacao = self.tipoDeOrientacao
			ori.chave = self.nome
			self.listaOATeseDeDoutorado.append(ori)

		if tag=='orientacao-em-andamento-de-mestrado':
			self.achouOADissertacaoDeMestrado = 0

			ori = OrientacaoEmAndamento(self.idMembro)
			ori.nome = self.nome
			ori.tituloDoTrabalho = self.tituloDoTrabalho
			ori.ano = self.ano
			ori.instituicao = self.instituicao
			ori.agenciaDeFomento = self.agenciaDeFomento
			ori.tipoDeOrientacao = self.tipoDeOrientacao
			ori.chave = self.nome
			self.listaOADissertacaoDeMestrado.append(ori)

		if tag=='projeto-de-pesquisa':
			self.achouProjetoDePesquisa = 0
			projeto = ProjetoDePesquisa(self.idMembro)
			projeto.nome = self.nome
			projeto.descricao = self.descricao
			projeto.anoInicio = self.anoinicio
			projeto.anoConclusao = self.anofim
			projeto.chave = self.nome
			self.listaProjetoDePesquisa.append(projeto)

		if tag=='premio-titulo':
			self.achouPremioOuTitulo = 0
			premio = PremioOuTitulo(self.idMembro)
			premio.descricao = self.descricao 
			premio.ano = self.ano
			premio.chave = self.descricao
			self.listaPremioOuTitulo.append(premio)


# ------------------------------------------------------------------------ #
#def handle_data(self, dado):

# ---------------------------------------------------------------------------- #
def stripBlanks(s):
	return re.sub('\s+', ' ', s).strip()

def htmlentitydecode(s):                                                                               
	return re.sub('&(%s);' % '|'.join(name2codepoint),                                                 
		lambda m: chr(name2codepoint[m.group(1)]), s)   

